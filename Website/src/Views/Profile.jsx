import React from 'react'
import '../css/style.css'
import { compose } from 'redux';
import { connect } from 'react-redux';
import { Field, reduxForm, autofill } from 'redux-form';
import RenderField from '../Component/RenderField';
let userDetails = "";
class Signup extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            errorMessage: ''
        }
        this.handleFormSubmit = this
            .handleFormSubmit
            .bind(this);
        this.handleClick = this
            .handleClick
            .bind(this);

        this.resetClick = this
            .resetClick
            .bind(this);

        userDetails = JSON.parse(localStorage.getItem("data"));
        
        //console.log(userDetails);
        
        if (userDetails !== null) {
            this.loadData();
        }

    }
    componentDidMount() {

    }

    handleFormSubmit(e) {

        e.preventDefault();

    }
    loadData() {

        this
            .props
            .dispatch(autofill("wizard", "firstname", userDetails.firstname));
        this
            .props
            .dispatch(autofill("wizard", "lastname", userDetails.lastname));
        this
            .props
            .dispatch(autofill("wizard", "company", userDetails.company));
        this
            .props
            .dispatch(autofill("wizard", "department", userDetails.department));
        this
            .props
            .dispatch(autofill("wizard", "position", userDetails.position));
        this
            .props
            .dispatch(autofill("wizard", "email", userDetails.email));
    }
    handleClick() {
        var _this = this;
        userDetails = window
            .store
            .getState()
            .form
            .wizard
            .values;
        localStorage.setItem("data", JSON.stringify(userDetails));
        _this.resetFields();
        _this.setState({ errorMessage: "Record Saved Successfully!" });

    }

    resetClick() {
        this.resetFields();
        this.setState({ errorMessage: "" });
    }

    resetFields() {
        this
            .props
            .dispatch(autofill("wizard", "firstname", ''));
        this
            .props
            .dispatch(autofill("wizard", "lastname", ''));
        this
            .props
            .dispatch(autofill("wizard", "company", ''));
        this
            .props
            .dispatch(autofill("wizard", "department", ''));
        this
            .props
            .dispatch(autofill("wizard", "position", ''));
        this
            .props
            .dispatch(autofill("wizard", "email", ''));
    }

    render() {

        var content = (
            <div className="login-form">

                <div className="form-bg">
                    <div className="wrapper">
                        <div className="container container-block">
                            <div className="row">
                                <div className="sign-in">
                                    <h2>
                                        <span>
                                            <label className="control-label error" for="ln">{this.state.errorMessage}</label>
                                        </span>
                                    </h2>
                                    <form className="form-horizontal" id="signup" onSubmit={this.handleFormSubmit}>
                                        <fieldset>
                                            <div className="form-group">

                                                <div className="col-md-12">
                                                    <label className="control-label" for="ln">First Name</label>

                                                    <Field
                                                        component={RenderField}
                                                        id="firstname"
                                                        name="firstname"
                                                        type="text"
                                                        placeholder="First name"
                                                        className="form-control input-md" />
                                                </div>


                                            </div>

                                            <div className="form-group">
                                                <div className="col-md-12">
                                                    <label className="control-label" for="ln">Last Name</label>
                                                    <Field
                                                        component={RenderField}
                                                        id="lastname"
                                                        name="lastname"
                                                        type="text"
                                                        placeholder="Last name"
                                                        className="form-control input-md" />
                                                </div>
                                            </div>
                                            <div className="form-group">
                                                <div className="col-md-12">
                                                    <label className="control-label" for="ln">Company</label>
                                                    <Field
                                                        component={RenderField}
                                                        id="company"
                                                        name="company"
                                                        type="text"
                                                        placeholder="Company"
                                                        className="form-control input-md" />
                                                </div>
                                            </div>
                                            <div className="form-group">
                                                <div className="col-md-12">
                                                    <label className="control-label" for="ln">Department</label>
                                                    <Field
                                                        component={RenderField}
                                                        id="department"
                                                        name="department"
                                                        type="text"
                                                        placeholder="Department"
                                                        className="form-control input-md" />
                                                </div>
                                            </div>
                                            <div className="form-group">
                                                <div className="col-md-12">
                                                    <label className="control-label" for="ln">Position</label>
                                                    <Field
                                                        component={RenderField}
                                                        id="position"
                                                        name="position"
                                                        type="text"
                                                        placeholder="Position"
                                                        className="form-control input-md" />
                                                </div>
                                            </div>

                                            <div className="form-group">

                                                <div className="col-md-12">
                                                    <label className="control-label" for="ln">Email</label>
                                                    <Field
                                                        component={RenderField}
                                                        id="email"
                                                        name="email"
                                                        type="email"
                                                        placeholder="Email"
                                                        className="form-control input-md" />

                                                </div>
                                            </div>


                                            <div className="form-group">
                                                <div className="col-md-6">
                                                    <button
                                                        id="login"
                                                        name="submit"
                                                        className="btn btn-primary"
                                                        onClick={this
                                                            .handleClick
                                                            .bind(this, 'signup')}>
                                                        Sign Up&nbsp;
                                                </button>
                                                </div>
                                                <div className="col-md-6">
                                                    <button
                                                        id="login"
                                                        name="reset"
                                                        className="btn btn-primary"
                                                        onClick={this
                                                            .resetClick
                                                            .bind(this, 'signup')}>
                                                        Reset&nbsp;
                                                </button>
                                                </div>
                                            </div>

                                        </fieldset>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
        return (
            <div>
                {content}
            </div>

        )
    }
}

const mapStateToProps = (state) => {
    if (state.form.wizard !== undefined) {
        // console.log(state.form.wizard.values);
        return state.form.wizard.values;
    }
    return {};
};

export default compose(connect(mapStateToProps), reduxForm({ form: 'wizard', destroyOnUnmount: false, forceUnregisterOnUnmount: true }))(Signup);