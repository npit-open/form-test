const validate = values => {
    console.log(values);
    const errors = {};
    if (!values.FirstName) {
    errors.FirstName = 'First Name is required';
    }``
    if (!values.LastName) {
    errors.LastName = 'Last Name is required';
    }
    if (!values.Email) {
    errors.Email = 'Email is required';
    } 
    else if (!/^[A-Z0-9.%+-]+@[A-Z0-9.-]+.[A-Z]{2,4}$/i.test(values.Email)) 
    {
    errors.Email = 'Invalid email address';
    }

    if (!values.Mobile) {
    errors.Mobile = 'Phone Number is required';
    }
    else {
    values.Mobile = values
    .Mobile
    .replace(/_/g, "")
    .replace(/-/g, "");
    if (values.Mobile.length != 10) {
    errors.Mobile = 'Please enter 10 digit number';
    }
   
    }

    return errors;
    };
    export default validate;