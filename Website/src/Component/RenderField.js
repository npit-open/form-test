import React from 'react';

const RenderField = ({ input, placeholder, label, type, mask, className, meta: { touched, error } }) => ( 
    <div>
           
        <input {...input }
        placeholder = { placeholder }
        type = { type }
        mask = { mask }
        //required
        className = {className}
        checked = { input.value }
        />
         <div> {
            (touched && error) &&
                <div style = {{ 'fontSize': '12px', 'color': 'rgb(244, 67, 54)',marginLeft:'8px' } }>
                 { error } </div>}</div >
           
             </div>
        );

        export default RenderField;

        
                // <div style = {{ 'fontSize': '12px', 'color': 'rgb(244, 67, 54)' } }>
        