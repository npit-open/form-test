import './css/style.css'
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
//import App from './App';
import {Provider} from 'react-redux';
import store from './store';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import registerServiceWorker from './registerServiceWorker';
import Profile from './Views/Profile';

const rootEl = document.getElementById('root');

ReactDOM.render(
  <Provider store={store}>
  <Router>
    <Switch>
      <Route exact path='/' component={Profile}/>
   
    </Switch>
  </Router>
</Provider>, rootEl,);